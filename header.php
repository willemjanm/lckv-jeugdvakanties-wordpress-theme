<!doctype html>
<html>
<head>
<title><?php bloginfo('title'); wp_title(); ?></title>
<meta charset='utf-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="LCKV Jeugdvakanties: Een week op vakantie zonder je ouders!">

<meta property="og:image" content="<?php bloginfo('template_directory'); ?>/images/ogimage.png" />
<meta property="og:title" content="LCKV Jeugdvakanties" />
<meta property="og:description" content="Een week op vakantie zonder je ouders!" />

<link href='<?php bloginfo('stylesheet_url'); ?>' type='text/css' rel='stylesheet'>
<!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/ie-fallback.css" />
<![endif]-->

<?php wp_head(); ?>

<!--<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/lib/class.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/controller/stackblur.min.js"></script>
<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/modernizr.js'></script>-->
<!--<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/lckv.min.js'></script>-->
<script type='application/ld+json'>
{
	"@context"	: "//schema.org",
	"@type"		: "Organization",
	"name"		: "LCKV Jeugdvakanties",
	"url"		: "//www.lckv.nl",
	"logo"		: "<?php bloginfo('template_directory'); ?>/images/logos/Square-logo-red.jpg",
	"email"		: "vragen@lckv.nl",
	"address"	: {
					"@type": "PostalAddress",
					"addressLocality": "Leiden",
					"postalCode": "2324NE",
					"streetAddress": "Voorschoterweg 8E"
				},
	"sameAs"	: [
   					"//www.facebook.com/LCKVJeugdvakanties",
    				"//www.twitter.com/lckv"
  				]
}
</script>

<!-- Favicon Begin -->
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_directory'); ?>/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php bloginfo('template_directory'); ?>/images/favicon/manifest.json">

<meta name="application-name" content="LCKV Jeugdvakanties"/>
<meta name="msapplication-TileColor" content="#EF4023" />
<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-310x310.png" />
<meta name="msapplication-notification" content="frequency=30;polling-uri=//notifications.buildmypinnedsite.com/?feed=//www.lckv.nl/feed/&amp;id=1;polling-uri2=//notifications.buildmypinnedsite.com/?feed=//www.lckv.nl/feed/&amp;id=2;polling-uri3=//notifications.buildmypinnedsite.com/?feed=//www.lckv.nl/feed/&amp;id=3;polling-uri4=//notifications.buildmypinnedsite.com/?feed=//www.lckv.nl/feed/&amp;id=4;polling-uri5=//notifications.buildmypinnedsite.com/?feed=//www.lckv.nl/feed/&amp;id=5;cycle=1" />

<!-- Favicon Eind -->

</head>

<?php 
if(preg_match('/(?i)msie/', $_SERVER['HTTP_USER_AGENT'])) {
	echo "<body class='ie'>";
} else {
	echo "<body>";
}
?>
	<header>
		<section id='first_header'>
			<div id='mobiel_menu'></div>
			<figure id="header_logo">
				<a href='<?php echo home_url('/'); ?>'><img src='<?php bloginfo('template_directory'); ?>/images/logo_<?php echo str_replace("oranje", "rood", str_replace("parent-", "", get_theme_mod('primaire_kleur', 'rood'))); ?>.svg' id='header_logo_img' alt='LCKV Jeugdvakanties'></a>
			</figure>
			<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
				<input type="text" size="18" value="<?php echo wp_specialchars($s, 1); ?>" placeholder="Zoeken..." name="s" id="search_input" />
			</form>
			<nav id='hoofd_menu'>
				<?php wp_nav_menu( array( 'theme_location' => 'primary') ); ?>
			</nav>
		</section>
	</header>