<li>
	<div class='lckv_agenda_item'>
		<div class='lckv_agenda_datum'>
			<div class='lckv_agenda_dag'></div>
			<div class='lckv_agenda_maand'></div>
		</div>
		<div class='lckv_agenda_tekst'>
			<a href='#_LOCATIONURL'><h3 itemprop='name'>#_LOCATIONNAME</h3></a>
				#_LOCATIONADDRESS<br>
				#_LOCATIONPOSTCODE #_LOCATIONTOWN
		</div>
		<a href='#_LOCATIONURL' class='leesmeer meer-2'><img src='https://www.lckv.nl/wp-content/themes/lckv_2015/images/pijl.svg' alt='lees meer' /></a>
	</div>
</li>