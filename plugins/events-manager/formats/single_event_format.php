<!-- <link href="https://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
<script type="text/javascript">(function () {
	if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
	if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
		var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
		s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
		s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
		var h = d[g]('body')[0];h.appendChild(s); }})();
		</script>
 -->
<div class='special-header special-header-2-cols'>
	<div class='lckv_headerGroup'>
		<div class='lckv_headerLabel'>
			<b>Datum/Tijd</b>
		</div>
		<div class='lckv_headerValue'>
			 
		</div>
		<div class='lckv_headerLabel'>
			<b>Start:</b>
		</div>
		<div class='lckv_headerValue'>
			#d-#m-#Y #H:#i
		</div>
		<div class='lckv_headerLabel'>
			<b>Einde:</b>
		</div>
		<div class='lckv_headerValue'>
			#@d-#@m-#@Y #@H:#@i
		</div>
	</div>

	<div class='lckv_headerGroup'>
		{has_location}
		<div class='lckv_headerLabel'>
			<b>Locatie</b>
		</div>
		<div class='lckv_headerValue'>
			 
		</div>
		<a href='#_LOCATIONURL'><b>#_LOCATIONNAME</b></a><br>
		#_LOCATIONADDRESS<br>
		#_LOCATIONTOWN
		{/has_location}
		 
	</div>
	<!--
	<div class='lckv_headerGroup' style='text-align: center;'>
		<div class='lckv_headerLabel'> </div>
		<div class='lckv_headerValue'> </div>
		<span class="addtocalendar atc-style-blue">
			<var class="atc_event">
				<var class="atc_date_start">#Y-#m-#d #H:#i:#s</var>
				<var class="atc_date_end">#@Y-#@m-#@d #@H:#@i:#@s</var>
				<var class="atc_timezone">Europe/Amsterdam</var>
				<var class="atc_title">#_EVENTNAME</var>
				<var class="atc_description"></var>
				<var class="atc_location">#_LOCATIONNAME</var>
				<var class="atc_organizer"></var>
				<var class="atc_organizer_email"></var>
			</var>
		</span>
	</div>
	-->	
</div>

#_EVENTNOTES

{has_location}
<div style="width:100%; margin-top: 40px;">#_LOCATIONMAP</div>
{/has_location}