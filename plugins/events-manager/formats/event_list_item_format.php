<li itemscope itemtype="http://schema.org/Event">
	<div class='lckv_agenda_item'>
		<div class='lckv_agenda_datum'>
			<div class='lckv_agenda_dag'>#d</div>
			<div class='lckv_agenda_maand'>#M</div>
		</div>
		<div class='lckv_agenda_tekst'>
			<a itemprop='url' href='#_EVENTURL'><h3 itemprop='name'>#_EVENTNAME</h3></a>
			{has_location}
			<span itemprop='location' itemscope itemtype='http://schema.org/Place'>
				<meta itemprop='url' content='#_LOCATIONURL'>
				Locatie: <span itemprop='name'>#_LOCATIONNAME</span>, <span itemprop='address'>#_LOCATIONADDRESS</span>
			</span><br />
			{/has_location}
				<meta itemprop='startDate' content='#Y-#m-#dT#H:#i'>
				<meta itemprop='endDate' content='#@Y-#@m-#@d T#@H:#@i'>
			{has_time}
				#_EVENTTIMES uur
			{/has_time}
			{no_time}
				#_EVENTTIMES uur
			{/no_time}
		</div>
		<a href='#_EVENTURL' class='leesmeer meer-2'><img src='https://www.lckv.nl/wp-content/themes/lckv_2015/images/pijl.svg' alt='lees meer' /></a>
	</div>
</li>