<?php
global $post;
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
	<main>
		<article id='vervolg_content'>
			<h1 class='page_title'><?php 
			if(is_shop()) {
				echo woocommerce_page_title();
			} else {
				echo get_the_title($post->post_parent);
			}
			?></h1><?php edit_post_link( '', '', '', $post->ID); ?> 
			<figure class='page_header'>
				<?php
				$page_id = wc_get_page_id('shop');
				if ( has_post_thumbnail() ) {
					echo "
						<div class='blur_background blur' >
							<img src='".wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'single-post-thumbnail')[0]."' style='filter: url(\"#blur\");' class='blur_background_img'>
						</div>

						<svg version='1.1 xmlns'http:www.w3.org/2000/svg' xmlns:xlink='http:www.w3.org/1999/xlink'>
						<defs>
						    <filter id='blur'>
						     	<feGaussianBlur stdDeviation='30'/>
						    </filter>
						</defs>
						</svg>
					<img src='".wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'single-post-thumbnail')[0]."' class='page_header_img'>";
				} else {
					echo "<img src='".get_bloginfo('template_directory')."/images/placeholder_page.jpg'>";
				}
				?>
			</figure>
		<section class='page_content_tekst woocommerce'>
