<?php
global $post;
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>


		</section>
		</article>

		<section id='pagina_navigatie' class='verberg_tablet verberg_mobiel'>
			<h1><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?> </h1>
				<ul>
					<?php
					  if($post->post_parent) {
					  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0&depth=1");
					  }

					  else {
					  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0&depth=1");
					  }
					  if ($children) { 
					  	$children_arr = explode("\n", $children);
					  	if(count($children_arr) > 8) {
					  		$max = 8;
					  	} else {
					  		$max = count($children_arr);
					  	}
					  	for($i=0; $i<$max; $i++) {
					  		echo $children_arr[$i];
					  	}
					  }
					  ?>
				</ul>
		</section>
		<section id='pagina_navigatie'>
			Winkelwagen
<?php do_action( 'woocommerce_cart_contents' ); ?>
		</section>
	</main>