<?php
/*
Template Name: Posts Loop
*/
?>

<?php get_header(); ?>

	<main>
			<article id='vervolg_content'>
				<h1>Zoekresultaten voor: <?php echo get_search_query(); ?></h1>
				<figure class='page_header'>
					<div class='blur_background blur' >
						<img src='<?php echo get_bloginfo('template_directory'); ?>/images/placeholder_zoeken.jpg' style='filter: url(\"#blur\");' class='blur_background_img'>
					</div>
					<img src='<?php echo get_bloginfo('template_directory'); ?>/images/placeholder_zoeken.jpg'>
				</figure>
			</article>

			<section id='pagina_navigatie' class='verberg_tablet verberg_mobiel'>
				<h1>Zoeken</h1>
			</section>
			<section class='page_content_posts_loop' id='home_nieuws	'>

            <?php if ( have_posts() ) : ?>

               <section id='home_nieuws'>
			<div class='nieuws_wrapper'>

                <?php 
                $zoeken =& new WP_Query("s=$s & showposts=-1");
				$aantal = $zoeken->post_count;
				$per_pagina = get_option('posts_per_page');
                while ( have_posts() ) { 
                	the_post();
	                echo "
					<article class='home_samenvatting' onClick=\"location.href='".get_permalink()."'\">
						<figure class='home_nieuws_thumb'>";
							if(has_post_thumbnail()) {
								the_post_thumbnail('home-samenvatting-post-thumbnail'); 
							} else {
								$datum_dag = get_the_date('d');
								if($datum_dag <= 8) { $color = "red"; }
								if($datum_dag > 8 && $dag <= 16) { $color = "yellow"; }
								if($datum_dag > 16 && $dag <= 24) { $color = "blue"; }
								if($datum_dag > 24 && $dag <= 31) { $color = "green"; }
								echo "<img width='240' height='180' src='".get_stylesheet_directory_uri()."/images/logos/Square-logo-".$color.".jpg' class='attachment-home-samenvatting-post-thumbnail wp-post-image' alt='LCKV Logo'>";
							}
							echo "
						</figure>
						<div class='home_samenvatting_wrapper'>
							<a href='".get_permalink()."'><h1>".get_the_title()."</h1></a>
							".get_the_excerpt();
							echo "
						</div>
						<a href='".get_permalink()."' class='leesmeer meer-1'>
						<img src='".get_bloginfo('template_directory')."/images/pijl.svg'></a>
					</article>";
	                }
	                echo "
	            </div>
	        </section>

	        <div class='navigation-box'>";
			
			$next = get_next_posts_link(); 
			$prev = get_previous_posts_link(); 
			if($prev != "") {
				 echo "<div class='archive-page-navigation navigation-prev'>".$prev."</div>";
			}
			if($next != "") {
				 echo "<div class='archive-page-navigation navigation-next'>".$next."</div>";
			}
            endif; ?>
			</div>
			</section>
	</main>

<?php get_footer(); ?>
<?php // get_sidebar(); ?>