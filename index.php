<?php get_header(); ?>
	<header id='second_header'>
		<figure class='foto' style="background-image: URL('<?php echo get_theme_mod('LCKV_2015_header_image', get_bloginfo('template_directory') . '/images/header.jpg'); ?>')">&nbsp;</figure>
		<h1 class='quote'><?php echo get_theme_mod('header-text', 'LCKV Jeugdvakanties'); ?></h1>
		<?php $button =  get_theme_mod('header-button', ""); 
		if($button != "") {
			echo "<a href='" . get_theme_mod('header-button-url', '#') ."' target='_blank' class='button'>".$button."</a>";
		} else {
			echo "<a href='" . get_theme_mod('header-button-url', '#') ."' target='_blank' class='button' style='display: none;'>".$button."</a>";
		}
		?>
	</header>

	<main>
		<section id='home_nieuws' class='nieuwe_sectie'>
			<h2>Nieuws &amp; Mededelingen</h2>
			<div class='nieuws_wrapper'>

				<?php 
				while(have_posts()): the_post() ?>

				<div class='home_samenvatting' onClick="location.href='<?php the_permalink(); ?>'">
					<figure class='home_nieuws_thumb'>
						<?php if(has_post_thumbnail()) {
							the_post_thumbnail('home-samenvatting-post-thumbnail'); 
						} else {
							$datum_dag = get_the_date('d');
							if($datum_dag <= 8) { $color = "red"; }
							if($datum_dag > 8 && $dag <= 16) { $color = "yellow"; }
							if($datum_dag > 16 && $dag <= 24) { $color = "blue"; }
							if($datum_dag > 24 && $dag <= 31) { $color = "green"; }
							echo "<img width='240' height='180' src='".get_stylesheet_directory_uri()."/images/logos/Square-logo-".$color.".jpg' class='attachment-home-samenvatting-post-thumbnail wp-post-image' alt='LCKV Logo'>";
						}?>
					</figure>
					<article class='home_samenvatting_wrapper'>
						<a href='<?php the_permalink(); ?>'><h3><?php the_title(); ?></h3></a>
						<?php the_excerpt(); ?>
					</article>
					<a href='<?php the_permalink(); ?>' class='leesmeer meer-1'><img src='<?php bloginfo('template_directory'); ?>/images/pijl.svg' alt='pijl'></a>
				</div>
				<?php endwhile; ?>
			</div>
			<?php if ( is_active_sidebar( 'sidebar-onder-homepage' )) {
				echo "<section id='sidebar_home_2' class='nieuwe_sectie'>";
			} else {
				echo "<section id='sidebar_home_2'>";
			}
			?>
				<?php get_sidebar('onder'); ?>
			</section>
		</section>
<?php if ( is_active_sidebar( 'sidebar-rechts-homepage' )) {
		echo "<section id='sidebar_home_1' class='nieuwe_sectie'>";
	} else {
		echo "<section id='sidebar_home_1'>";
	}
	?>
			<?php get_sidebar('rechts'); ?>

		</section>
<?php if ( is_active_sidebar( 'sidebar-social-homepage' )) {
		echo "<section id='sidebar_home_3' class='nieuwe_sectie'>";
	} else {
		echo "<section id='sidebar_home_3'>";
	}
	?>
			<?php get_sidebar('onder2'); ?>
		</section>
	</main>

<?php get_footer(); ?>