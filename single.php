<?php get_header(); ?>

	<main>
		<?php while(have_posts()): the_post(); ?>
			<article id='vervolg_content'>
				<h1 class='page_title'><?php the_title(); ?></h1><?php edit_post_link( '', $before, $after, $id ); ?> 
				<!-- <span class='post_date'><?php the_date('d m Y'); ?></span> -->
				<figure class='page_header'>
					<?php
					if ( has_post_thumbnail() ) {
						echo "
							<div class='blur_background blur' >
								<img src='".wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0]."' style='filter: url(\"#blur\");' class='blur_background_img' alt='".get_the_title()."'>
							</div>

							<svg version='1.1 xmlns'http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
							<defs>
							    <filter id='blur'>
							     	<feGaussianBlur stdDeviation='30'/>
							    </filter>
							</defs>
							</svg>
						<img src='".wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0]."' class='page_header_img' alt='".get_the_title()."'>";
					} else {
						echo "<img src='".get_bloginfo('template_directory')."/images/placeholder_page.jpg'>";
					}
					?>
				</figure>
				<section class='page_content_tekst'>
					<?php the_content(); ?>

				</section>
			</article>

			<section id='pagina_navigatie' class='verberg_tablet verberg_mobiel'>
				<h1>Nieuws</h1>
					<ul>
					<?php
					$actief = get_the_ID();
					$args = array(
						'numberposts' => 5,
						'offset' => 0,
						'category' => 0,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_type' => 'post',
						'post_status' => 'publish',
						'suppress_filters' => true 
					);
					$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
					foreach($recent_posts AS $recent) {
						echo "<li><a href='".get_permalink($recent['ID'])."'"; if($actief == $recent['ID']) { echo " class='actief'"; } echo ">".$recent['post_title']."</a></li>";
					}
					?>
					</ul>
			</section>
		<?php endwhile; ?>
	</main>

<?php get_footer(); ?>
<?php // get_sidebar(); ?>