/**********************************************************/
/************************* MENU ***************************/
/**********************************************************/


jQuery(document).ready( function() {

	jQuery("#mobiel_menu").click( function() {
		var zichtbaar = jQuery("#hoofd_menu").css('display');

		if(zichtbaar == "none") {
			jQuery("#hoofd_menu").addClass('show');
			jQuery("#first_header").addClass('fixed');
			jQuery("#second_header").addClass('extra_margin');
		} else {
			jQuery("#hoofd_menu").removeClass('show');
			jQuery("#first_header").removeClass('fixed');
			jQuery("#second_header").removeClass('extra_margin');

			jQuery(".page_item_has_children").removeClass('menuActief');
			jQuery(".children").css("display", "none");

		}
	});
	jQuery(".page_item_has_children").hover(function() {
		var child 			= $jQuery(this).children(".children");
		var child_right		= parseFloat($jQuery(child).offset().left) + parseFloat($jQuery(child).innerWidth());
		var child_margin	= parseFloat($jQuery(child).css("margin-left").replace("-", ""));
		var child_overflow 	= parseFloat(parseFloat(child_right) - $jQuery(window).width());

		if(child_overflow > 0 && $jQuery(this).parent().hasClass('children')) {
			$jQuery(child).addClass("menuToLeft");
		} else {
			child_overflow = child_overflow + child_margin;
		}

		if(child_overflow > 0 && !$jQuery(this).parent().hasClass('children')) {
			$jQuery(child).css("margin-left", "-"+child_overflow+"px");
		}
	});
});

var isMobile = { 
	Android: function() { return navigator.userAgent.match(/Android/i); }, 
	BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i); }, 
	iOS: function() { return navigator.userAgent.match(/iPhone|iPad|iPod/i); }, 
	Opera: function() { return navigator.userAgent.match(/Opera Mini/i); }, 
	Windows: function() { return navigator.userAgent.match(/IEMobile/i); }, 
	any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()); }, 
	anyButIos: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.Opera() || isMobile.Windows()); } 
};

jQuery(document).ready(function() {
	jQuery('.page_item_has_children').click(function(e) {
		if(isMobile.anyButIos() != null || jQuery(document).width() <= 650) {
			showSubMenu(jQuery(this), e);
		}
	});
});


function showSubMenu(element, e) {
	var linkClass = element.attr('class');

	if(element.hasClass('menuActief') == false) {
		e.preventDefault();


		//verberg alle andere submenu's
		//maar niet als submenu de parent is

		var subSubMenu = element.parent().parent().hasClass('menuActief');
		if(subSubMenu == false) {

			jQuery(".page_item_has_children").removeClass('menuActief');
			jQuery('.children').removeClass('show');
		}

		element.children("ul").addClass('show');
		element.addClass('menuActief');

	}
}

/**********************************************************/
/*********************** BLUR EFFECT **********************/
/**********************************************************/

jQuery(document).ready( function() {
	if(jQuery(".blur_background_img").length) {
		if( 
		!Modernizr.cssfilters && // Not a browser with CSS filter support
		!( $jQuery('html').hasClass('lt-ie9') ) && // Not an old version of IE (which support MS filters)
		!(typeof InstallTrigger !== 'undefined') // Not Firefox (which supports SVG filters)
		){
		var img = jQuery(".blur_background_img").attr('src');
		jQuery(".blur_background_img").css('display', 'none');
			var blur = new Blur({ 
				el : document.querySelector('.blur'), 
				path : img, 
				radius : 30, 
				fullscreen : true,
				});
		}
	}
});

jQuery(document).ready(function() {
	jQuery("select").wrap("<div class='styled-select'></div>");
});

/**********************************************************/
/********************* TEKST WIDGET ***********************/
/**********************************************************/

function lckv_tekst_tab_show_tab(tabblad) {
	jQuery(".lckv_tekst_tabblad").css('display', 'none');
	jQuery("#lckv_tekst_tab_" + tabblad).css('display', 'block');
}
jQuery(document).ready(function() {
	jQuery(".lckv_tekst .tabs h2:first-child").addClass('actief');
	jQuery('.lckv_tekst h2').click(function(e) {
		var actiefId = e.target.id.replace("_tab_tab_", "_tab_");

		jQuery(".lckv_tekst h2").removeClass("actief");
		jQuery("#" + e.target.id).addClass("actief");
		jQuery(".lckv_tekst_tabblad").css("display", "none");
		jQuery("#" + actiefId).css("display", "block");
	});
});
