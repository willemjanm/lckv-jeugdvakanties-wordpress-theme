var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var token = function(length) {
	var str = rand() + rand(); // to make it longer
    return str.substring(0,length);
};

function replaceAll(string, find, replace) {
  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function lckv_tekst_widget_add_tab(aantal_field, field_name, title_name) {
	var aantal = jQuery("#" + aantal_field).val();
//	alert(aantal);
	var aantal_nieuw = parseInt(aantal) + 1;
	jQuery("#" + aantal_field).val(aantal_nieuw);
	
	var tabs_html = jQuery(".lckv_tekst_tabbladen").html()
	
	jQuery(".lckv_tekst_tabbladen").append("Titel<br><input type='text' name='"+title_name+"["+aantal_nieuw+"]'><br>Tekst<br><textarea name='"+field_name+"["+aantal_nieuw+"]' class='widefat' style='height: 150px;'></textarea><hr>");
	
}

function lckv_media_widget_replace(element, tekst_name, veld_id, aantal) {
	var id = token(5);

	element = replaceAll(element, "[VELD_ID]", veld_id+"-field-"+id);
	element = replaceAll(element, "[VELD_ID2]", veld_id);
	element = replaceAll(element, "[AANTAL]", aantal);
	element = replaceAll(element, "[VELDNAAM]", tekst_name);
	element = replaceAll(element, "[VELDTEKST]", "");
	element = replaceAll(element, "[VELDICON]", "lees");
	element = replaceAll(element, "[VELDID]", id);
	element = replaceAll(element, "[OP_button_lees]", "opacity: 1;");
	element = replaceAll(element, "[OP_button_tekst]", "opacity: 0.4;");
	element = replaceAll(element, "[OP_button_media]", "opacity: 0.4;");
	element = replaceAll(element, "[OP_button_downloads]", "opacity: 0.4;");
	element = replaceAll(element, "[sel_none]", "checked='checked'");
	element = replaceAll(element, "[sel_WP_page]", "");
	element = replaceAll(element, "[sel_URL]", "");
	element = replaceAll(element, "[VELDURL]", "");
	element = replaceAll(element, "[WP_PAGE]", "");
	return element;
}

function lckv_media_widget_add_item(tekst_name, veld_id, phpToken) {
	var aantal = jQuery(".lckv_media_items-"+phpToken+" ."+veld_id+"-media-item").length + 1;
	var duplicate = jQuery(".LCKV_2015_media_widget_duplicate").html();
	var dropdown = jQuery(".LCKV_2015_media_widget_dropdown_duplicate").html();
	dropdown = lckv_media_widget_replace(dropdown, tekst_name, veld_id, aantal);
	duplicate = replaceAll(duplicate, "[WP_PAGE_DROPDOWN]", dropdown);
	
	duplicate = lckv_media_widget_replace(duplicate, tekst_name, veld_id, aantal);
	jQuery(".lckv_media_items-"+phpToken).append(duplicate);	
}

function lckv_media_widget_delete_item(aantal_field, field) {
	var aantal = jQuery("#" + aantal_field).val();
	var aantal_nieuw = parseInt(aantal) - 1
	jQuery("#" + aantal_field).val(aantal_nieuw);
	jQuery("#"+field).remove();
}

function lckv_media_set_icon(icon_field, icon, active) {
	jQuery("."+icon_field+"-icon").val(icon);
	jQuery("."+icon_field+"-icon-button").css("opacity", "0.4");
	jQuery(active).css("opacity", "1");
}