( function( $ ) {
	wp.customize( 'primaire_kleur', function( value ) {
		value.bind( function( newval ) {
			var logo_kleur = newval.replace("parent-", "");
			if(logo_kleur == "oranje") { logo_kleur = "rood" }
//alert(logo_kleur);

			var full_url = jQuery("#scheme-parent").attr('href');
			var schemes_url = full_url.substring(0, full_url.lastIndexOf("/") + 1);
			jQuery("#scheme-parent").attr('href', schemes_url+newval+'.css');


			var logo_full_url = jQuery("#header_logo_img").attr('src');
			var logo_url = logo_full_url.substring(0, logo_full_url.lastIndexOf("/") + 1);
			jQuery("#header_logo_img").attr('src', logo_url+"logo_"+logo_kleur+'.svg');
		} );
	} );

	wp.customize( 'secundaire_kleur', function( value ) {
		value.bind( function( newval ) {
			var full_url = jQuery("#scheme-child").attr('href');
			var schemes_url = full_url.substring(0, full_url.lastIndexOf("/") + 1);
			jQuery("#scheme-child").attr('href', schemes_url+newval+'.css');
		} );
	} );

	wp.customize( 'header-text', function( value ) {
		value.bind( function( newval ) {
			jQuery(".quote").text(newval);
		} );
	} );

	wp.customize( 'LCKV_2015_header_image', function( value ) {
		value.bind( function( newval ) {
			jQuery("#second_header .foto").css("background-image", "URL("+newval+")");
		} );
	} );

	wp.customize( 'header-button', function( value ) {
		value.bind( function( newval ) {
			if(newval == "") {
				jQuery("#second_header .button").css('display', 'none');
				jQuery("#second_header .button").text(newval);
			} else {
				jQuery("#second_header .button").css('display', 'inline-block');
				jQuery("#second_header .button").text(newval);
			}
		} );
	} );

	wp.customize( 'header-button-url', function( value ) {
		value.bind( function( newval ) {
			jQuery("#second_header .button").attr('src', newval);
		} );
	} );

} )( jQuery )