<?php
/*
Template Name: Posts Loop
*/
?>

<?php get_header(); ?>

	<main>
		<?php while(have_posts()): the_post(); ?>
			<article id='vervolg_content'>
				<h1><?php the_title(); ?></h1>
				<figure class='page_header'>
					<?php
					if ( has_post_thumbnail() ) {
						echo "
							<div class='blur_background blur' >
								<img src='".wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0]."' style='filter: url(\"#blur\");' class='blur_background_img'>
							</div>

							<svg version='1.1 xmlns'http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
							<defs>
							    <filter id='blur'>
							     	<feGaussianBlur stdDeviation='30'/>
							    </filter>
							</defs>
							</svg>
						<img src='".wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0]."' class='page_header_img'>";
					} else {
						echo "<img src='".get_bloginfo('template_directory')."/images/placeholder_page.jpg'>";
					}
					?>
				</figure>
			</article>

			<section id='pagina_navigatie' class='verberg_tablet verberg_mobiel'>
				<h1><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?> </h1>
					<ul>
						<?php
						  if($post->post_parent) {
						  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0&depth=1");
						  }

						  else {
						  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0&depth=1");
						  }
						  if ($children) { 
						  	$children_arr = explode("\n", $children);
						  	if(count($children_arr) > 8) {
						  		$max = 8;
						  	} else {
						  		$max = count($children_arr);
						  	}
						  	for($i=0; $i<$max; $i++) {
						  		echo $children_arr[$i];
						  	}
						  }
						  ?>
					</ul>
			</section>
			<section class='page_content_posts_loop' id='home_nieuws'>

				<?php the_content(); ?>

			</section>
		<?php endwhile; ?>
	</main>

<?php get_footer(); ?>
<?php // get_sidebar(); ?>