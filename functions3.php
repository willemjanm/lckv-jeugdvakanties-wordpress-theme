<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

add_theme_support('post-thumbnails');
add_image_size('pagina-header', 760);
add_image_size('home-samenvatting-post-thumbnail', 270, 180, true);

add_filter('jpeg_quality', create_function('', 'return 100;'));

add_theme_support( 'menus' );
add_action( 'after_setup_theme', 'register_my_menu' );

/**
 * Include the file with the ThemeUpdater class.
 */
require 'update-checker/plugin-update-checker.php';
  
$myUpdateChecker = Puc_v4p3_Factory::buildUpdateChecker(
	'https://gitlab.com/willemjanm/lckv-jeugdvakanties-wordpress-theme/',
	__FILE__,
	'lckv_2015'
);
$myUpdateChecker->setAuthentication('ddz8tCWZhenssyxzEvo5');
$myUpdateChecker->setBranch('master');

function register_my_menu() {
  register_nav_menu( 'primary', 'Hoofd Menu' );
}

function lckv_customizer_js()
{
	echo "<script src='".get_bloginfo('template_directory')."/js/customizer.js' type='text/javascript'></script>";
}

function lckv_get_scheme_settings($file)
{
	$myfile = fopen($file, "r") or die("Unable to open file! " . $file);
	$css = fread($myfile,filesize($file));

	preg_match_all('/\/\*(.+?)\*\//s', $css, $matches);
	foreach ($matches[1] AS $comment) {
		if (preg_match("/--LCKV Theme Settings--/", $comment)) {
			$lines = explode("\n", $comment);
			foreach ($lines AS $line) {
				$key   = str_replace(":", "", strtok($line, " "));
				$value = substr($line, strpos($line, " "));
				if (!preg_match("/Theme Settings/", $value) && $value != "") {
					$settings[$key] = $value;
				}
			}
		}
	}
	return $settings;
}

function get_primaire_kleur() {
	$schemes_url = str_replace(get_bloginfo('url') . "/", "", get_bloginfo('template_directory')) . "/css/schemes/".get_theme_mod('primaire_kleur', 'parent-oranje').".css";
	$settings = lckv_get_scheme_settings($schemes_url);
	return $settings['Voorbeeld-kleur'];

}

function lckv_customize_register($wp_customize)
{


	class Example_Customize_Textarea_Control extends WP_Customize_Control
	{
		public $type = 'radio_html';

        public function render_content() {
        	echo "<label class='customize-control-title'>
        		<span class='customize-control- title'>" . esc_html($this->label) ."</span>
        	</label>";

        	foreach ($this->choices AS $naam => $kleur) {
        		echo "<label for='".$naam."' style='display: inline-block; margin-right: 10px; text-align: center;'>
        			<input type='radio' value='".$naam."' id='".$naam."' name='_customize-html-radio-".$this->id."' data-customize-setting-link='".$this->id."'".$selected." style='margin-right: 0px; margin-bottom: 4px;'><br>
        			".$kleur."
        		</label>";
        	} 
        }
    }

    $kleuren_primair = "";

	$schemes_url = ".." . str_replace(get_bloginfo('url'), "", get_bloginfo('template_directory')) . "/css/schemes";
	if(is_array(glob($schemes_url . "/parent-*.css"))) {
		foreach (glob($schemes_url . "/parent-*.css") as $file_primair) {
			$kleur_primair = str_replace($schemes_url."/", "", $file_primair);
			$kleur_primair = str_replace(".css", "", $kleur_primair);
			$kleuren_primair[$kleur_primair] = "<div style='width: 30px; height: 30px; border-radius: 50%; background-color: ".lckv_get_scheme_settings($file_primair)['Voorbeeld-kleur']."; display: inline-block;'></div>";
		}
	}

	$wp_customize->add_section('lckv_color_scheme', array(
		'title' => 'Kleurenschema',
		'description' => '',
		'priority' => 120
		));

	$wp_customize->add_setting('primaire_kleur', array(
		'default' => 'parent-oranje',
        'transport' => 'postMessage',
		));

	$wp_customize->add_control(new Example_Customize_Textarea_Control($wp_customize, 'primaire_kleur', array(
		'type' => 'radio_html',
		'label' => 'Primaire Kleur',
		'section' => 'lckv_color_scheme',
		'settings' => 'primaire_kleur',
		'choices' => $kleuren_primair
	)));

	$kleuren_secundair = "";

	if(is_array(glob($schemes_url . "/child-*.css"))) {
		foreach (glob($schemes_url . "/child-*.css") as $file_secundair) {
			$kleur_secundair = str_replace($schemes_url."/", "", $file_secundair);
			$kleur_secundair = str_replace(".css", "", $kleur_secundair);
			$kleuren_secundair[$kleur_secundair] = "<div style='width: 30px; height: 30px; border-radius: 50%; background-color: ".lckv_get_scheme_settings($file_secundair)['Voorbeeld-kleur']."; display: inline-block;'></div>";
		}
	}

	$defaults = array(
		'primaire_kleur' => 'parent-oranje',
		'secundaire_kleur' => 'child-blauw'
	);

	$wp_customize->add_setting('secundaire_kleur', array(
        'transport' => 'postMessage',
		));

	$wp_customize->add_control(new Example_Customize_Textarea_Control($wp_customize, 'secundaire_kleur', array(
		'type' => 'radio_html',
		'label' => 'Secundaire Kleur',
		'section' => 'lckv_color_scheme',
		'settings' => 'secundaire_kleur',
		'naam' => 'secundaire_kleur',
		'choices' => $kleuren_secundair
	)));


	$wp_customize->add_setting(
		'LCKV_2015_header_image', array(
		    'default' => get_bloginfo('template_directory') . '/images/header.jpg',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control( new Multi_Image_Custom_Control($wp_customize, 
		'LCKV_2015_header_image', array(
		    'label'    => __('Header Afbeelding', 'LCKV_2015'),
		    'section'  => 'lckv_header',
		    'settings' => 'LCKV_2015_header_image',
		)
	));

	$wp_customize->add_section('lckv_header', array(
		'title' => 'Header',
		'description' => '',
		'priority' => 120
		));

	$wp_customize->add_setting(
	    'header-text', array(
	    	'default' => 'LCKV Jeugdvakanties',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-text',
        array(
            'label' => 'Header Tekst',
            'section' => 'lckv_header',
            'settings' => 'header-text',
            'type' => 'text',
        )
	);

	$wp_customize->add_setting(
	    'header-slide-duration', array(
	    	'default' => '8',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-slide-duration',
        array(
            'label' => 'Slide duur',
            'section' => 'lckv_header',
            'settings' => 'header-slide-duration',
            'type' => 'number',
        )
	);

	$wp_customize->add_setting(
	    'header-button', array(
	    	'default' => '',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-button',
        array(
            'label' => 'Button',
            'section' => 'lckv_header',
            'settings' => 'header-button',
            'type' => 'text',
        )
	);


	$wp_customize->add_setting(
	    'header-button-url', array(
	    	'default' => '',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-button-url',
        array(
            'label' => 'Button Link',
            'section' => 'lckv_header',
            'settings' => 'header-button-url',
            'type' => 'text',
        )
	);

	$wp_customize->add_setting(
	    'header-video-enabled', array(
	    	'default' => '',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-video-enabled',
        array(
            'label' => 'Video weergeven',
            'section' => 'lckv_header',
            'settings' => 'header-video-enabled',
            'type' => 'checkbox',
        )
	);

	$wp_customize->add_setting(
	    'header-video-url', array(
	    	'default' => '',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-video-url',
        array(
            'label' 	=> 'Youtube video ID',
            'section' 	=> 'lckv_header',
            'settings' 	=> 'header-video-url',
            'type' 		=> 'text',
        )
	);

	$wp_customize->add_setting(
	    'header-video-sound', array(
	    	'default' => 'dempen',
		    'transport' => 'postMessage',
		)
	);
	 
	$wp_customize->add_control(
        'header-video-sound',
        array(
            'label' 	=> 'Geluid',
            'section' 	=> 'lckv_header',
            'settings' 	=> 'header-video-sound',
            'type' 		=> 'select',
            'choices' 	=> array('Dempen', 'Hover', 'Altijd aan')
            //'choices' 	=> array('Dempen', 'Hover', 'Klikken', 'Altijd aan')
        )
	);


	$wp_customize->add_panel( 'panel_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Example Panel', 'textdomain' ),
	    'description' => __( 'Description of what this panel does.', 'textdomain' ),
	) );

	$wp_customize->add_section( 'section_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Example Section', 'textdomain' ),
	    'description' => '',
	    'panel' => 'panel_id',
	) );

}

if (!class_exists('WP_Customize_Image_Control')) {

} else {

	class Multi_Image_Custom_Control extends WP_Customize_Control
	{
	    public function enqueue()
	    {
	      wp_enqueue_style('multi-image-style', get_template_directory_uri().'/css/multi-image.css');
	      wp_enqueue_script('multi-image-script', get_template_directory_uri().'/js/multi-image.js', array( 'jquery' ), rand(), true);
	    }

	    public function render_content()
	    { ?>
	          <label>
	            <span class='customize-control-title'>Afbeeldingen</span>
	          </label>
	          <div>
	            <ul class='images'></ul>
	          </div>
	          <div class='actions'>
	            <a class="button-secondary upload">Toevoegen</a>
	          </div>

	          <input class="wp-editor-area" id="images-input" type="hidden" <?php $this->link(); ?>>
	      <?php
	    }
	}
}
add_action('customize_register', 'lckv_customize_register');

function lckv_page_menu_args($args)
{
	$args['show_home'] = true;
	return $args;
}
add_filter('wp_page_menu_args', 'lckv_page_menu_args');

register_sidebar(array(
	'name' => 'Sidebar rechts homepage',
	'id' => 'sidebar-rechts-homepage',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h2>',
	'after_title' => '</h2>'
	));

register_sidebar(array(
	'name' => 'Sidebar Onder Nieuws',
	'id' => 'sidebar-onder-homepage',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h2>',
	'after_title' => '</h2>'
	));

register_sidebar(array(
	'name' => 'Sidebar Social Media',
	'id' => 'sidebar-social-homepage',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h2>',
	'after_title' => '</h2>'
	));

function lckv_customizer_live_preview()
{
	wp_enqueue_script( 
		  'lckv-themecustomizer',									//Give the script an ID
		  get_template_directory_uri().'/js/theme-customizer.js',	//Point to file
		  array( 'jquery','customize-preview' ),					//Define dependencies
		  '1.0',													//Define a version (optional) 
		  true														//Put script in footer?
	);
}

function LCKV_2015_admin_javascript()
{
	echo "<script type='text/javascript' src='" . get_bloginfo('template_directory') . "/js/lckv_admin.js'></script>";
}

function lckv_add_color_schemes() {
	echo "<link href='". get_bloginfo('template_directory')."/css/schemes/".get_theme_mod('primaire_kleur', 'parent-oranje').".css' type='text/css' rel='stylesheet' id='scheme-parent'>
		  <link href='". get_bloginfo('template_directory')."/css/schemes/".get_theme_mod('secundaire_kleur', 'child-blauw').".css' type='text/css' rel='stylesheet' id='scheme-child'>";
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}


/****************************************/
/************ NIEUWS ARCHIEF ************/
/****************************************/

function LCKV_2015_nieuws_archief( $atts ){

	$page = get_query_var( 'page', 1 );
	if($page < 1) { $page = 1; }
	$numposts = $atts['num_posts'];
	$category = get_cat_ID( $atts['cat'] );
	$offset = ($page - 1) * $numposts;

	if($category == "") {
		$category = 0;
	}

	$args = array(
	    'numberposts' => $numposts,
	    'offset' => $offset,
	    'category' => $category,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'suppress_filters' => true 
	);
    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

    foreach($recent_posts AS $recent) {

		if($recent['post_excerpt'] == "") {
			$samenvatting = $recent['post_content'];
			if (preg_match('/^.{1,140}\b/s', $recent['post_content'], $match)) {
			    $samenvatting=$match[0] . "...";
			}
		} else {
			$samenvatting = $recent['post_excerpt'];
		}

    	$thumb = get_the_post_thumbnail($recent['ID'], 'home-samenvatting-post-thumbnail');
    	echo "
    	<article class='home_samenvatting' onClick=\"location.href='".get_permalink($recent['ID'])."'\">
    		<figure class='home_nieuws_thumb'>".$thumb;
				if($thumb != "") {
					get_the_post_thumbnail('home-samenvatting-post-thumbnail', $recent['ID']); 
				} else {
					$datum_dag = get_the_date('d', $recent['ID']);
					if($datum_dag <= 8) { $color = "red"; }
					if($datum_dag > 8 && $dag <= 16) { $color = "yellow"; }
					if($datum_dag > 16 && $dag <= 24) { $color = "blue"; }
					if($datum_dag > 24 && $dag <= 31) { $color = "green"; }
					echo "<img width='240' height='180' src='".get_stylesheet_directory_uri()."/images/logos/Square-logo-".$color.".jpg' class='attachment-home-samenvatting-post-thumbnail wp-post-image' alt='LCKV Logo'>";
				}
				echo "
			</figure>
			<div class='home_samenvatting_wrapper'>
				<a href='".get_permalink($recent['ID'])."'><h2>".$recent['post_title']."</h2></a>
				".$samenvatting."
			</div>
			<a href='".get_permalink($recent['ID'])."' class='leesmeer meer-1'><img src='".get_bloginfo('template_directory')."/images/pijl.svg' alt='Lees meer'></a>
    	</article>";
    }
    $next = $page + 1;
    $prev = $page - 1;
    echo "<div class='navigation-box'>";
    	if($page > 1) {
	    	echo "<a href='".get_permalink()."/".$prev."' class='archive-page-navigation navigation-prev'>Vorige</a>";
	    }

		$count_posts = wp_count_posts();
		if(ceil($count_posts->publish / $numposts) > $page) {
		    echo "<a href='".get_permalink()."/".$next."' class='archive-page-navigation navigation-next'>Volgende</a>";
		}
    echo "</div>";

}

function lckv_die_handler( $message, $title = '', $args = array() ) {

	return "lckv_error_page";

}

function lckv_error_page() {
	$defaults = array( 'response' => 500 );
	$r = wp_parse_args($args, $defaults);

	$have_gettext = function_exists('__');

	if ( function_exists( 'is_wp_error' ) && is_wp_error( $message ) ) {
		if ( empty( $title ) ) {
			$error_data = $message->get_error_data();
			if ( is_array( $error_data ) && isset( $error_data['title'] ) )
				$title = $error_data['title'];
		}
		$errors = $message->get_error_messages();
		switch ( count( $errors ) ) {
		case 0 :
			$message = '';
			break;
		case 1 :
			$message = "<p>{$errors[0]}</p>";
			break;
		default :
			$message = "<ul>\n\t\t<li>" . join( "</li>\n\t\t<li>", $errors ) . "</li>\n\t</ul>";
			break;
		}
	} elseif ( is_string( $message ) ) {
		$message = "<p>$message</p>";
	}

	if ( isset( $r['back_link'] ) && $r['back_link'] ) {
		$back_text = $have_gettext? __('&laquo; Back') : '&laquo; Back';
		$message .= "\n<p><a href='javascript:history.back()'>$back_text</a></p>";
	}

	if ( ! did_action( 'admin_head' ) ) {
		if ( !headers_sent() ) {
			status_header( $r['response'] );
			nocache_headers();
			header( 'Content-Type: text/html; charset=utf-8' );
		}

		if ( empty($title) ) {
			$title = $have_gettext ? __('WordPress &rsaquo; Error') : 'WordPress &rsaquo; Error';
		}

		$text_direction = 'ltr';
		if ( isset($r['text_direction']) && 'rtl' == $r['text_direction'] ) {
			$text_direction = 'rtl';
		}
		elseif ( function_exists( 'is_rtl' ) && is_rtl() ) {
			$text_direction = 'rtl';
		}
	}
}

function lckv_add_scripts() {
	wp_enqueue_script('modernizr_js', get_template_directory_uri().'/js/modernizr.js', array(), '1.0', true);
	wp_enqueue_script('lckv_js', get_template_directory_uri().'/js/lckv.js', array(), '1.2', true);
	wp_enqueue_script('class_js', get_template_directory_uri().'/js/lib/class.js', array(), '1.0', true);
	wp_enqueue_script('stackblur_js', get_template_directory_uri().'/js/controller/stackblur.min.js', array(), '1.0', true);
}

function my_em_custom_formats( $array ){
	$my_formats[] = 'dbem_event_list_item_format'; //the format you want to override, corresponding to file above.
	$my_formats[] = 'dbem_single_event_format'; //the format you want to override, corresponding to file above.
	$my_formats[] = 'dbem_single_location_format'; //the format you want to override, corresponding to file above.
	$my_formats[] = 'dbem_location_list_item_format'; //the format you want to override, corresponding to file above.
	return $array + $my_formats; //return the default array and your formats.
}


function load_dashicons_front_end() {
	wp_enqueue_style( 'dashicons' );
}

function custom_edit_post_link($output) {
 $output = str_replace('class="post-edit-link"', 'class="post-edit-link dashicons dashicons-edit"', $output);
 return $output;
}

// woocommerce support

add_filter( 'woocommerce_enqueue_styles', '__return_false' );
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    remove_action('woocommerce_sidebar','woocommerce_get_sidebar',10);
    remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);

}

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
/**
 * WooCommerce Loop Product Thumbs
 **/
 if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
	function woocommerce_template_loop_product_thumbnail() {
		echo woocommerce_get_product_thumbnail();
	} 
 }
/**
 * WooCommerce Product Thumbnail
 **/
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	
	function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
		global $post, $woocommerce;

		$output = '<div class="lckv_wc_shop_wrapper_thumb">';
		if ( has_post_thumbnail() ) {
			
			$output .= get_the_post_thumbnail( $post->ID, $size ); 
			
		} else {
		
			$output .= '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder" width="' . $placeholder_width . '" height="' . $placeholder_height . '" />';
		
		}
		
		$output .= '</div>';
		
		return $output;
	}
}

add_filter('edit_post_link', 'custom_edit_post_link');
add_filter('em_formats_filter', 'my_em_custom_formats', 1, 1);
add_filter('wp_die_handler', 'lckv_die_handler');
add_filter('widget_text','do_shortcode');

add_shortcode( 'LCKV-nieuws-archief', 'LCKV_2015_nieuws_archief' );

add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
add_action('customize_preview_init', 'lckv_customizer_live_preview' );
add_action('admin_head', 'LCKV_2015_admin_javascript');
add_action('wp_head', 'lckv_add_color_schemes');
add_action('wp_head', 'lckv_add_scripts');

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

include('LCKV_inc/files.php');

?>