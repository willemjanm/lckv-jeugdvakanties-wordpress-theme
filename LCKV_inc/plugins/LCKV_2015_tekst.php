<?php

add_action('widgets_init', array(
	'lckv_tekst_tab_widget_2015',
	'register'
	));

if (!defined('2015_lckv_tekst_URL')) {
	define('2015_lckv_tekst_URL', plugin_dir_url(__FILE__));
}

class lckv_tekst_tab_widget_2015 extends WP_Widget
	{
	/**
	* Constructor.
	*/
	public function __construct()
	{
		$widget_ops = array(
			'classname' => 'lckv_tekst',
			'description' => 'Voeg een tekstbox met meerdere tabbladen toe'
			);
		parent::__construct(strtolower(__CLASS__), '2015 LCKV Tekst', $widget_ops);
	}

	/**
	* Echo the settings update form
	*
	* @param array $instance Current settings
	*/
	public function form($instance)
	{
		$aantal = isset($instance['aantal']) ? $instance['aantal'] : '';
		$aantal = esc_attr($aantal);

		$aantal2 = count($titel);

		print_r($titel);
		?>
		<input type="hidden" name="<?php echo $this->get_field_name('aantal'); ?>" 
		id="<?php echo $this->get_field_id('aantal'); ?>" 
		value="<?php echo attribute_escape($aantal); ?>">
		</p>
		<?php

		$fields         = isset($instance['fields']) ? $instance['fields'] : array();
		$field_num      = count($fields);
		$fields_html    = array();
		$fields_counter = 0;

		$titels        = isset($instance['titel']) ? $instance['titel'] : array();
		$titel_num     = count($titel);
		$titel_html    = array();
		$titel_counter = 1;


		echo "<div class='lckv_tekst_tabbladen'>";

		foreach ($titels as $name => $value) {
			$titel_html[] = sprintf('Titel<br><input type="text" name="%1$s[%3$s]" value="%4$s" class="widefat">
				Tekst<br><textarea name="%2$s[%3$s]" value="%5$s" class="widefat" style="height: 150px;">%5$s</textarea><hr>', $this->get_field_name('titel'), $this->get_field_name('fields'), $fields_counter, esc_attr($value), esc_attr($fields[$name]));
			$fields_counter++;
		}

		print join('<br />', $titel_html);
		echo "</div>";
		echo "<a href='javascript:void(0)' onClick=\"lckv_tekst_widget_add_tab('" . $this->get_field_id('aantal') . "', '" . $this->get_field_name('fields') . "', '" . $this->get_field_name('titel') . "')\">Tabblad toevoegen</a>";
	}

	/**
	* Renders the output.
	*
	* @see WP_Widget::widget()
	*/
	public function widget($args, $instance)
	{
		$i      = 1;
		$aantal = count($instance['titel']);
		echo $args['before_widget'] . "<div class='tabs'>";
		foreach ($instance['titel'] AS $titel) {
			echo "<h2 id='lckv_tekst_tab_tab_" . $i . "'>" . $titel . "</h2>";
			$i++;
		}
		echo "</div>
		<div class='content'>";
		$i = 1;
		foreach ($instance['fields'] AS $tekst) {
			echo "<div class='lckv_tekst_tabblad' id='lckv_tekst_tab_" . $i . "' style='height: 100%;";
			if ($i != 1) {
				echo " display: none;";
			}
			echo "'>" . $tekst . "</div>";
			$i++;
		}
		echo "</div>";
		echo $args['after_widget'];
	}

	/**
	* Prepares the content. Not.
	*
	* @param  array $new_instance New content
	* @param  array $old_instance Old content
	* @return array New content
	*/
	public function update($new_instance, $old_instance)
	{
		$instance           = $old_instance;
	//        $instance['titel'] = esc_html( $new_instance['titel'] );
		$instance['aantal'] = count($new_instance['fields']);

		$instance['fields'] = array();

		if (isset($new_instance['fields'])) {
			foreach ($new_instance['fields'] as $value) {
				if ('' !== trim($value))
					$instance['fields'][] = $value;
			}
		}

		$instance['titel'] = array();

		if (isset($new_instance['titel'])) {
			foreach ($new_instance['titel'] as $value2) {
				if ('' !== trim($value2))
					$instance['titel'][] = $value2;
			}
		}

		return $instance;
	}

	/**
	* Tell WP we want to use this widget.
	*
	* @wp-hook widgets_init
	* @return void
	*/
	public static function register()
	{
		register_widget(__CLASS__);
	}
}