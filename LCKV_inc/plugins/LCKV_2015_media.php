<?php

class LCKV_2015_media_widget extends WP_Widget {
	function LCKV_2015_media_widget() {
		$widget_ops = array(
			'classname' => 'LCKV_2015_media_widget',
			'description' => __('Voeg media items toe als widget ', 'LCKV_2015_media_widget')
			);

		$control_ops = array(
			'width' => 200,
			'height' => 350,
			'id_base' => '2015-lckv-media'
			);

		$this->WP_Widget('2015-lckv-media', __('2015 LCKV Media', '2015 LCKV Media'), $widget_ops, $control_ops);
	}


	function form($instance) {

		$token = substr(sha1(md5(rand(0, 999))), 0, 5);

		$defaults = array(
			"titel" => "Media"
		);

		echo "<label for='".$this->get_field_id('titel')."'><b>Titel</b></label><br>
		<input type='text' class='widefat' name='".$this->get_field_name('titel')."' value='".$instance['titel']."' id='".$this->get_field_id('titel')."'>
		<br><br>
		<div class='lckv_media_items-".$token."'>";

		$div = "
			<div class='widget open [VELD_ID2]-media-item' id='[VELD_ID]'>
				<div class='widget-top'>
					<div class='widget-title ui-sortable-handle'>
						<h4>Tekst [AANTAL]</h4>
					</div>
				</div>
				<div class='widget-inside'>
					Tekst<br>
					<input class='widefat' type='text' name='[VELDNAAM][[VELDID]][tekst]' value='[VELDTEKST]'>
					<input class='widefat [VELDID]-icon' type='hidden' name='[VELDNAAM][[VELDID]][icon]' value='[VELDICON]'>
					<input class='widefat' type='hidden' name='[VELDNAAM][[VELDID]][id]' value='[VELDID]'>
					<br><br>
					<b>Verwijst naar:</b><br>
					<input type='radio' name='[VELDNAAM][[VELDID]][linktype]' id='[VELDID]-linktype-none' value='none' [SEL_none]><label for='[VELDID]-linktype-none'>Geen verwijzing</label><br>
					<input type='radio' name='[VELDNAAM][[VELDID]][linktype]' id='[VELDID]-linktype-WP_page' value='WP_page' [SEL_WP_page]><label for='[VELDID]-linktype-WP_page'>Pagina: [WP_PAGE_DROPDOWN]</label><br>
					<input type='radio' name='[VELDNAAM][[VELDID]][linktype]' id='[VELDID]-linktype-URL' value='URL' [SEL_URL]><label for='[VELDID]-linktype-URL'>URL <input type='text' name='[VELDNAAM][[VELDID]][URL]' value='[VELDURL]'></label>
					<br><br>
					<b>Icoon:</b><br>
					<img src='".get_bloginfo('template_directory')."/images/lees-zwart.svg' class='[VELDID]-icon-button' width='30px' style='margin-right: 15px; [OP_button_lees] cursor: pointer;' onClick=\"javascript:lckv_media_set_icon('[VELDID]', 'lees', this)\" alt='lees'>
					<img src='".get_bloginfo('template_directory')."/images/tekst-zwart.svg' class='[VELDID]-icon-button' width='30px' style='margin-right:15px; [OP_button_tekst] cursor: pointer;' onClick=\"javascript:lckv_media_set_icon('[VELDID]', 'tekst', this)\" alt='tekst'>
					<img src='".get_bloginfo('template_directory')."/images/media-zwart.svg' class='[VELDID]-icon-button' width='30px' style='margin-right:15px; [OP_button_media] cursor: pointer;' onClick=\"javascript:lckv_media_set_icon('[VELDID]', 'media', this)\" alt='media'>
					<img src='".get_bloginfo('template_directory')."/images/downloads-zwart.svg' class='[VELDID]-icon-button' width='30px' style='[OP_button_downloads] cursor: pointer;' onClick=\"javascript:lckv_media_set_icon('[VELDID]', 'downloads', this)\" alt='download'>
					<br><br>
					<a href='javascript:void(0)' onClick=\"javascript:lckv_media_widget_delete_item('".$VELDID_veld."', '[VELD_ID]')\">Verwijderen</a>
				</div>
			</div>
		";

		echo "
		<div class='LCKV_2015_media_widget_duplicate' style='display: none'>".$div."</div>
		<div class='LCKV_2015_media_widget_dropdown_duplicate' style='display: none'>".wp_dropdown_pages(array('name' => '[VELDNAAM][[VELDID]][WP_page]', 'echo' => false))."</div>";

		$i = 1;
		if(!is_array($instance['tekst'])) { $media_item = array(); } else { $media_item = $instance['tekst']; }
		foreach($media_item AS $tekst) {

			$args_dropdown = array(
				'name' => '[VELDNAAM][[VELDID]][WP_page]',
				'selected' => $tekst['WP_page'],
				'echo' => false
			);

			$OP_lees 		= ($tekst['icon'] == "lees" ? "opacity: 1; " : "opacity: 0.4;");
			$OP_tekst 		= ($tekst['icon'] == "tekst" ? "opacity: 1; " : "opacity: 0.4;");
			$OP_media 		= ($tekst['icon'] == "media" ? "opacity: 1; " : "opacity: 0.4;");
			$OP_downloads 	= ($tekst['icon'] == "downloads" ? "opacity: 1; " : "opacity: 0.4;");

			$SEL_none 		= ($tekst['linktype'] == "none" ? "checked='checked'" : "");
			$SEL_WP_page 	= ($tekst['linktype'] == "WP_page" ? "checked='checked'" : "");
			$SEL_URL 		= ($tekst['linktype'] == "URL" ? "checked='checked'" : "");

			$veld = str_replace("[WP_PAGE_DROPDOWN]", wp_dropdown_pages($args_dropdown), $div);
			$veld = str_replace("[AANTAL]", $i, $veld);
			$veld = str_replace("[VELD_ID]", $this->get_field_id('tekst')."-field-".$tekst['id'], $veld);
			$veld = str_replace("[VELD_ID2]", $this->get_field_id('tekst'), $veld);
			$veld = str_replace("[VELDNAAM]", $this->get_field_name('tekst'), $veld);
			$veld = str_replace("[VELDTEKST]", $tekst['tekst'], $veld);
			$veld = str_replace("[VELDICON]", $tekst['icon'], $veld);
			$veld = str_replace("[VELDID]", $tekst['id'], $veld);
			$veld = str_replace("[OP_button_lees]", $OP_lees, $veld);
			$veld = str_replace("[OP_button_tekst]", $OP_tekst, $veld);
			$veld = str_replace("[OP_button_media]", $OP_media, $veld);
			$veld = str_replace("[OP_button_downloads]", $OP_downloads, $veld);
			$veld = str_replace("[SEL_none]", $SEL_none, $veld);
			$veld = str_replace("[SEL_WP_page]", $SEL_WP_page, $veld);
			$veld = str_replace("[SEL_URL]", $SEL_URL, $veld);
			$veld = str_replace("[VELDURL]", $tekst['URL'], $veld);
			$veld = str_replace("[WP_PAGE]", $tekst['WP_page'], $veld);

			echo $veld;
			$i++;
		}

		echo "</div>
		<br><a href='javascript:void(0)' onClick=\"lckv_media_widget_add_item('". $this->get_field_name('tekst') . "', '" . $this->get_field_id('tekst') . "', '".$token."')\">Item toevoegen</a>";
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['titel'] = $new_instance['titel'];
		$instance['tekst'] = $new_instance['tekst'];

		return $instance;
	}

	function widget($args, $instance) {
		extract($args);

		if(!is_array($instance['tekst'])) {
			$instance['tekst'] = array();
		}

		echo $before_widget . $before_title . $instance['titel'] . $after_title;

		foreach($instance['tekst'] AS $tekst) {

			switch($tekst['linktype']) {
				case "none":
					$link = "#";
				break;
				case "WP_page":
					$link = get_permalink($tekst['WP_page']);
				break;
				case "URL":
					if (!preg_match("~^(?:f|ht)tps?://~i", $tekst['URL'])) {
						$link = "http://" . $tekst['URL'];
					} else { 
						$link = $tekst['URL'];
					}
				break;
			}

			echo "
				<div class='lckv_media_item' onClick=\"location.href='".$link."'\">
					<figure class='lckv_media_icon'><img src='".get_bloginfo('template_directory')."/images/".$tekst['icon'].".svg' alt='".$tekst['icon']."'></figure>
					<div class='lckv_media_tekst'>".$tekst['tekst']."</div>
					<a href='".$link."' class='leesmeer meer-1'><img src='".get_bloginfo('template_directory')."/images/pijl.svg' alt='Lees meer'></a>
				</div>";
		}

		echo $after_widget;
	}
}

add_action("widgets_init", function() {
	register_widget('LCKV_2015_media_widget');
	}
);
?>