<?php
if (!defined('2015_groene_pijl_URL')) {
	define('2015_groene_pijl_URL', plugin_dir_url(__FILE__));
}

class RandomPostWidget2 extends WP_Widget
{
	function RandomPostWidget2()
	{
		$widget_ops = array(
			'classname' => 'groene_pijl verberg_tablet verberg_mobiel',
			'description' => 'Voeg eenvoudig de groene pijl toe als verwijzing'
			);
		$this->WP_Widget('2015_LCKVGroenePijl', '2015 Groene Pijl', $widget_ops);
	}

	function form($instance)
	{
		$instance = wp_parse_args((array) $instance, array(
			'title' => '',
			'tekst' => '',
			'groene_pijl_pagina1' => '',
			'groene_pijl_pagina2' => '',
			'groene_pijl_pagina3' => 'pagina',
			'target' => 'self'
			));
		$title    = $instance['title'];
		$tekst    = $instance['tekst'];
		$pagina1  = $instance['groene_pijl_pagina1'];
		$pagina2  = $instance['groene_pijl_pagina2'];
		$pagina3  = $instance['groene_pijl_pagina3'];
		$target   = $instance['target'];
		?>
		<p><label for="<?php
		echo $this->get_field_id('title');
		?>">Titel: <input class="widefat" id="<?php
		echo $this->get_field_id('title');
		?>" name="<?php
		echo $this->get_field_name('title');
		?>" type="text" value="<?php
		echo attribute_escape($title);
		?>" /></label><br>
		<label for="<?php
		echo $this->get_field_id('tekst');
		?>">Tekst: <input class="widefat" id="<?php
		echo $this->get_field_id('tekst');
		?>" name="<?php
		echo $this->get_field_name('tekst');
		?>" type="text" value="<?php
		echo attribute_escape($tekst);
		?>" /></label></p>

		<hr>
		<b>Verwijst naar:</b><br><br>

		<input type='radio' name="<?php
		echo $this->get_field_name('groene_pijl_pagina3');
		?>" value='geen' <?php
		if ($pagina3 == "geen") {
			echo "checked='checked'";
		}
		?>>Geen verwijzing<br>
		<input type='radio' name="<?php
		echo $this->get_field_name('groene_pijl_pagina3');
		?>" value='pagina' <?php
		if ($pagina3 == "pagina") {
			echo "checked='checked'";
		}
		?>>Pagina

		<?php

		$args = array(
			'name' => $this->get_field_name('groene_pijl_pagina1'),
			'id' => 'groene_pijl_pagina1',
			'selected' => $pagina1
			);

		wp_dropdown_pages($args);
		?><br>

		<input type='radio' name="<?php
		echo $this->get_field_name('groene_pijl_pagina3');
		?>" value='url' <?php
		if ($pagina3 == "url") {
			echo "checked='checked'";
		}
		?>>URL
		<input  id="<?php
		echo $this->get_field_id('groene_pijl_pagina2');
		?>" name="<?php
		echo $this->get_field_name('groene_pijl_pagina2');
		?>" type="text" value="<?php
		echo attribute_escape($pagina2);
		?>" />
		<br><br><hr>
		<b>Openen in:</b><br><br>
		<input type='radio' name="<?php
		echo $this->get_field_name('target');
		?>" value='self' <?php
		if ($target == "self") {
			echo "checked='checked'";
		}
		?>>Zelfde venster<br>
		<input type='radio' name="<?php
		echo $this->get_field_name('target');
		?>" value='blank' <?php
		if ($target == "blank") {
			echo "checked='checked'";
		}
		?>>Nieuw venster
		<br><br><hr>

		<?php
	}

	function update($new_instance, $old_instance)
	{
		$instance                        = $old_instance;
		$instance['title']               = $new_instance['title'];
		$instance['tekst']               = $new_instance['tekst'];
		$instance['groene_pijl_pagina1'] = $new_instance['groene_pijl_pagina1'];
		$instance['groene_pijl_pagina2'] = $new_instance['groene_pijl_pagina2'];
		$instance['groene_pijl_pagina3'] = $new_instance['groene_pijl_pagina3'];
		$instance['target']              = $new_instance['target'];
		return $instance;
	}

	function widget($args, $instance)
	{
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$title   = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$tekst   = empty($instance['tekst']) ? ' ' : apply_filters('widget_tekst', $instance['tekst']);
		$pagina1 = empty($instance['groene_pijl_pagina1']) ? ' ' : apply_filters('widget_groene_pijl_pagina', $instance['groene_pijl_pagina1']);
		$pagina2 = empty($instance['groene_pijl_pagina2']) ? ' ' : apply_filters('widget_groene_pijl_pagina', $instance['groene_pijl_pagina2']);
		$pagina3 = empty($instance['groene_pijl_pagina3']) ? ' ' : apply_filters('widget_groene_pijl_pagina', $instance['groene_pijl_pagina3']);
		$target  = empty($instance['target']) ? ' ' : apply_filters('widget_target', $instance['target']);

		if (!empty($title))
			echo $before_title . $title . $after_title;
		;

// WIDGET CODE GOES HERE
		echo "<div class='groene_pijl_bg'";

		if ($pagina3 == 'geen') {
			echo " style='cursor: default;'";
		}
		if ($pagina3 == 'pagina') {
			if ($target == 'blank') {
				echo " onClick=\"window.open('" . get_permalink($pagina1) . "', '_blank')\"";
			} else {
				echo " onClick=\"location.href='" . get_permalink($pagina1) . "'\"";
			}
		}
		if ($pagina3 == 'url') {
			if ($target == 'blank') {
				echo " onClick=\"window.open('" . $pagina2 . "', '_blank')\"";
			} else {
				echo " onClick=\"location.href='" . $pagina2 . "'\"";
			}
		}

		echo ">$tekst</div>";

		echo $after_widget;
	}

}

add_action('widgets_init', create_function('', 'return register_widget("RandomPostWidget2");'));
