<?php

class LCKV_2015_facebook_widget extends WP_Widget {
	function LCKV_2015_facebook_widget() {
		$widget_ops = array(
			'classname' => 'LCKV_2015_facebook_widget',
			'description' => __('Likebox van de LCKV Facebook ', 'LCKV_2015_facebook_widget')
			);

		$control_ops = array(
			'width' => 200,
			'height' => 350,
			'id_base' => '2015-lckv-facebook'
			);

		$this->WP_Widget('2015-lckv-facebook', __('2015 LCKV Facebook', '2015 LCKV Facebook'), $widget_ops, $control_ops);
	}


	function form($instance) {
		$defaults = array(
			'faces' => 'true',
			'small-header' => 'false',
			'hide-cover-photo' => 'false',
			'page-posts' => 'false'
		);
		$faces = ($instance['faces'] == "true" ? "checked='checked'" : "");
		$small_header = ($instance['small-header'] == "true" ? "checked='checked'" : "");
		$hide_cover_photo = ($instance['hide-cover-photo'] == "true" ? "checked='checked'" : "");
		$page_posts = ($instance['page-posts'] == "true" ? "checked='checked'" : "");

		echo "
			<p>
				<input class='widefat' type='checkbox' id='".$this->get_field_id( 'faces' )."' value='true' name='".$this->get_field_name( 'faces' )."' ".$faces.">
				<label for='".$this->get_field_id( 'faces' )."'>Toon Gezichten van vrienden</label><br>

				<input class='widefat' type='checkbox' id='".$this->get_field_id( 'small-header' )."' value='true' name='".$this->get_field_name( 'small-header' )."' ".$small_header.">
				<label for='".$this->get_field_id( 'small-header' )."'>Kleine header</label><br>

				<input class='widefat' type='checkbox' id='".$this->get_field_id( 'hide-cover-photo' )."' value='true' name='".$this->get_field_name( 'hide-cover-photo' )."' ".$hide_cover_photo.">
				<label for='".$this->get_field_id( 'hide-cover-photo' )."'>Verberg omslagfoto</label><br>

				<input class='widefat' type='checkbox' id='".$this->get_field_id( 'page-posts' )."' value='true' name='".$this->get_field_name( 'page-posts' )."' ".$page_posts.">
				<label for='".$this->get_field_id( 'page-posts' )."'>Toon pagina berichten</label><br>


			</p>
		";
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance[ 'faces' ] = strip_tags( $new_instance[ 'faces' ] );
		$instance[ 'small-header' ] = strip_tags( $new_instance[ 'small-header' ] );
		$instance[ 'hide-cover-photo' ] = strip_tags( $new_instance[ 'hide-cover-photo' ] );
		$instance[ 'page-posts' ] = strip_tags( $new_instance[ 'page-posts' ] );
		return $instance;
	}

	function widget($args, $instance) {
		extract($args);

		$faces = ($instance['faces'] == "true" ? "data-show-facepile='true'" : "data-show-facepile='false'");
		$small_header = ($instance['small-header'] == "true" ? "data-small-header='true'" : "data-small-header='false'");
		$hide_cover_photo = ($instance['hide-cover-photo'] == "true" ? "data-hide-cover='true'" : "data-hide-cover='false'");
		$page_posts = ($instance['page-posts'] == "true" ? "data-show-posts='true'" : "data-show-posts='false'");

		echo $before_widget . $before_title."Like ons op Facebook".$after_title.

		"
		<section>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = \"//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.4\";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div class='fb-page' data-href='https://www.facebook.com/LCKVjeugdvakanties' data-width='500' data-height='220' ".$small_header." data-adapt-container-width='true' ".$hide_cover_photo." ".$faces." ".$page_posts."><div class='fb-xfbml-parse-ignore'><blockquote cite='https://www.facebook.com/LCKVjeugdvakanties'><a href='https://www.facebook.com/LCKVjeugdvakanties'>LCKV Jeugdvakanties</a></blockquote></div></div>
		</section>	

		".
		$after_widget;
	}
}

add_action("widgets_init", function() {
	register_widget('LCKV_2015_facebook_widget');
	}
);
?>