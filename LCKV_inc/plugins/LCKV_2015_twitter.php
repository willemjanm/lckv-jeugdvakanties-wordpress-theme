<?php

add_action('widgets_init', 'my_widget2');


function my_widget2()
{
	register_widget('MY_Widget2');

}

class MY_Widget2 extends WP_Widget
{

	function MY_Widget2()
	{
		$widget_ops = array(
			'classname' => 'example',
			'description' => __('Laat de laatste tweets van @lckv zien ', 'example')
			);

		$control_ops = array(
			'width' => 200,
			'height' => 350,
			'id_base' => '2015-lckv-twitter'
			);

		$this->WP_Widget('2015-lckv-twitter', __('2015 @LCKV tweets', '2015 LCKV Twitter'), $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($args);

//Our variables from the widget settings.
		$titel    = apply_filters('example', $instance['titel']);
		$tweets   = apply_filters('example', $instance['tweets']);
		$baseUrl2 = plugin_dir_url(__FILE__);

		echo $before_widget;
		echo $before_title . $titel . $after_title;

		echo '
		<section class="twitter-widget">
		<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/LCKV" data-widget-id="519196392242171904" data-tweet-limit="' . $tweets . '" data-chrome="noheader nofooter transparent noborders">Tweets van @LCKV</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</section>';


		echo $after_widget;
	}

//Update the widget 

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

//Strip tags from title and name to remove HTML 
		$instance['titel']  = strip_tags($new_instance['titel']);
		$instance['tweets'] = strip_tags($new_instance['tweets']);

		return $instance;
	}


	function form($instance)
	{

//Set up some default widget settings.
		$defaults = array(
			'tweets' => __('2', 'example'),
			'titel' => __('@LCKV', 'example')
			);
		$instance = wp_parse_args((array) $instance, $defaults);
		?>
		<p>
			<label for="<?php
			echo $this->get_field_id('titel');
			?>"><?php
			_e('Titel:', 'tweets');
			?></label>
			<input id="<?php
			echo $this->get_field_id('titel');
			?>" name="<?php
			echo $this->get_field_name('titel');
			?>" value="<?php
			echo $instance['titel'];
			?>" />

			<label for="<?php echo $this->get_field_id('tweets'); ?>"><?php
			_e('Aantal Tweets:', 'tweets');
			?></label>
			<input id="<?php
			echo $this->get_field_id('tweets');
			?>" name="<?php
			echo $this->get_field_name('tweets');
			?>" value="<?php
			echo $instance['tweets'];
			?>" style="width:30px;" />
		</p>

		<?php
	}
}

?>