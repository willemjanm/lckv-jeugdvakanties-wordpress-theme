	<footer>
		<section id='footer_navigatie'>
			<?php

			$args = array(
				'parent' => 0,
				'sort_order' => 'ASC',
				'sort_column' => 'menu_order'
			);

			$pages = get_pages($args);
			$i = 1;
				foreach($pages as $page) {
					if($i <= 6) {
						?>
						<section class='footerblok'>
							<a href="<?php echo get_page_link($page->ID) ?>" class='parent_page'><h3><?php echo $page->post_title ?></h3></a>
							<ul>
								<?php echo wp_list_pages("title_li=&child_of=".$page->ID."&echo=1&depth=1"); ?>
							</ul>
						</section>
						<?php
					}
					$i++;
				}
			?>
		</section>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>